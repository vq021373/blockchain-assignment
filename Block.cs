﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;

namespace BlockchainAssignment
{
    class Block
    {
        /* Block Variables */
        private DateTime timestamp; // Time of creation

        private int index, // Position of the block in the sequence of blocks
            difficulty = 4; // An arbitrary number of 0's to proceed a hash value

        public String prevHash, // A reference pointer to the previous block
            hash, // The current blocks "identity"
            merkleRoot,  // The merkle root of all transactions in the block
            minerAddress; // Public Key (Wallet Address) of the Miner

        public List<Transaction> transactionList; // List of transactions in this block

        public static List<double> blockTimes = new List<double>(); // Store the times it takes to mine blocks
        public static double targetBlockTime = 600; // Target block time in seconds (e.g., 600 for 10 minutes)

        public DateTime miningStartTime;
        public DateTime miningEndTime;



        // Proof-of-work
        public long nonce; // Number used once for Proof-of-Work and mining

        // Rewards
        public double reward; // Simple fixed reward established by "Coinbase"

        /* Genesis block constructor */
        public Block()
        {
            timestamp = DateTime.Now;
            index = 0;
            transactionList = new List<Transaction>();
            hash = Mine();
        }

        /* New Block constructor */
        public Block(Block lastBlock, List<Transaction> transactions, String minerAddress)
        {
            timestamp = DateTime.Now;

            index = lastBlock.index + 1;
            prevHash = lastBlock.hash;

            this.minerAddress = minerAddress; // The wallet to be credited the reward for the mining effort
            reward = 1.0; // Assign a simple fixed value reward
            transactions.Add(createRewardTransaction(transactions)); // Create and append the reward transaction
            transactionList = new List<Transaction>(transactions); // Assign provided transactions to the block

            merkleRoot = MerkleRoot(transactionList); // Calculate the merkle root of the blocks transactions
            hash = Mine(); // Conduct PoW to create a hash which meets the given difficulty requirement
        }

        /* Hashes the entire Block object */
        public String CreateHash()
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* Concatenate all of the blocks properties including nonce as to generate a new hash on each call */
            String input = timestamp.ToString() + index + prevHash + nonce + merkleRoot;

            /* Apply the hash function to the block as represented by the string "input" */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);
            
            return hash;
        }

        public string Mine()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            int threadCount = Environment.ProcessorCount; // Use one thread per processor
            int nonceRange = (int.MaxValue / threadCount); // Define a range of nonce values for each thread
            CancellationTokenSource cts = new CancellationTokenSource();
            string validHash = null;
            long validNonce = 0;

            // Define the work for each thread
            Action<object> mineAction = (object obj) =>
            {
                int threadIndex = (int)obj;
                long startNonce = threadIndex * nonceRange;
                long endNonce = (threadIndex == threadCount - 1) ? int.MaxValue : startNonce + nonceRange;
                for (long nonceAttempt = startNonce; nonceAttempt < endNonce && validHash == null; nonceAttempt++)
                {
                    if (cts.Token.IsCancellationRequested)
                        break;

                    string hashAttempt = CreateHashWithNonce(nonceAttempt);
                    if (hashAttempt.StartsWith(new string('0', difficulty)))
                    {
                        validHash = hashAttempt; // Assign valid hash found
                        validNonce = nonceAttempt; // Assign the nonce that led to a valid hash
                        cts.Cancel(); // Request cancellation to stop work on other threads
                        break;
                    }
                }

            };

            // Launch threads to perform mining
            Task[] tasks = new Task[threadCount];
            for (int i = 0; i < threadCount; i++)
            {
                tasks[i] = Task.Factory.StartNew(mineAction, i, cts.Token);
            }

            Task.WaitAll(tasks); // Wait for all threads to complete or for cancellation

            if (validHash != null)
            {
                nonce = validNonce; // Set the block's nonce to the valid nonce found
                return validHash; // Return the valid hash found
            }

            throw new InvalidOperationException("Failed to mine a block within the given nonce range.");
        }

        private string CreateHashWithNonce(long nonce)
        {
            SHA256 hasher = SHA256Managed.Create();

            // Concatenate your block's data with the nonce
            string input = $"{timestamp}{index}{prevHash}{nonce}{merkleRoot}";

            // Compute the hash
            byte[] hashBytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Convert the byte array to a hex string
            StringBuilder hash = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hash.Append(hashBytes[i].ToString("x2"));
            }

            return hash.ToString();
        }




        // Merkle Root Algorithm - Encodes transactions within a block into a single hash
        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"
            
            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.combineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i=0; i<hashes.Count; i+=2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.combineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }

        // Create reward for incentivising the mining of block
        public Transaction createRewardTransaction(List<Transaction> transactions)
        {
            double fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee); // Sum all transaction fees
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, ""); // Issue reward as a transaction in the new block
        }

        /* Concatenate all properties to output to the UI */
        public override string ToString()
        {
            return "[BLOCK START]"
                + "\nIndex: " + index
                + "\tTimestamp: " + timestamp
                + "\nPrevious Hash: " + prevHash
                + "\n-- PoW --"
                + "\nDifficulty Level: " + difficulty
                + "\nNonce: " + nonce
                + "\nHash: " + hash
                + "\n-- Rewards --"
                + "\nReward: " + reward
                + "\nMiners Address: " + minerAddress
                + "\n-- " + transactionList.Count + " Transactions --"
                +"\nMerkle Root: " + merkleRoot
                + "\n" + String.Join("\n", transactionList)
                + "\n[BLOCK END]";
        }


    }
}
